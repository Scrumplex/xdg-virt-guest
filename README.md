XDG Virt Guest
-------------------
Java endpoint for xdg-virt-open

# Installation
Download a release from [Releases](https://gitlab.com/Scrumplex/xdg-virt-guest/releases) and move it to the Autostart folder on Windows.

Start it once and edit the config file created in your user directory, called `.xdg-virt.ini`. You will need to add the host's IP address in the `[Whitelist]` section.

## Configuration example
```ini
[General]
port = 40333

[Whitelist]
address = 127.0.0.1
address = 192.168.122.1
```

# Security warning
Every host in the whitelist has full remote-execution access. Any application can be started through this daemon.
It would not be wise to forward the port outside of your own machine. 

# TODO
 - Add authentication to the protocol. Preferably utilizing GNU/Linux utilities like OpenSSL on the host
 - Create easy installation wizard for Windows
 - Package it up for Arch Linux
 - Configuration UI

# Requirements
 - jre8

# License
[GNU General Public License 3.0](LICENSE)
