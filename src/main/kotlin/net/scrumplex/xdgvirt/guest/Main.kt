package net.scrumplex.xdgvirt.guest

import org.ini4j.Ini
import java.awt.Desktop
import java.io.File
import java.net.URI

class Main {
    var conf: Ini
    private val server: Server

    init {
        val configFile = File(System.getProperty("user.home"), ".xdg-virt.ini")

        if (!configFile.exists()) {
            configFile.createNewFile()

            conf = Ini(configFile)

            val general = conf.add("General")

            general["port"] = 40333.toString()

            val whitelist = conf.add("Whitelist")
            whitelist["address"] = "127.0.0.1"

            conf.store()
        }

        conf = Ini(configFile)

        server = Server(this)
    }

    fun start() {
        server.listen()
    }
}


fun main() {
    Main().start()
}

fun browse(uri: URI): Boolean {
    val desktop = if (Desktop.isDesktopSupported()) Desktop.getDesktop() else null
    if (desktop != null && desktop.isSupported(Desktop.Action.BROWSE)) {
        desktop.browse(uri)
        return true
    }
    return false
}
