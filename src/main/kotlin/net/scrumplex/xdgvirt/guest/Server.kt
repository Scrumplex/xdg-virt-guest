package net.scrumplex.xdgvirt.guest

import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.ServerSocket
import java.net.URI
import java.util.concurrent.Executors
import javax.swing.JOptionPane

class Server(private val main: Main) {

    private val sock: ServerSocket
    private val executor = Executors.newFixedThreadPool(3)

    init {
        val general = main.conf.get("General")!!
        sock = ServerSocket(general.get("port", 0.javaClass))
    }

    fun listen() {
        val whitelist = main.conf.get("Whitelist")!!

        executor.execute {
            while (isRunning()) {
                val client = sock.accept()
                if (whitelist.containsValue(client.inetAddress.hostAddress)) {
                    executor.execute {
                        val reader = BufferedReader(InputStreamReader(client.getInputStream()))

                        val requestString = reader.readLine()

                        if (requestString != null) { // Ignore zero IO connections (e.g. netcat -z)
                            try {
                                val uri = URI(requestString)
                                browse(uri)
                            } catch (e: Exception) {
                                e.printStackTrace()
                                System.err.println("Invalid request: $requestString")
                                JOptionPane.showMessageDialog(
                                    null,
                                    "Invalid request from ${client.inetAddress.hostAddress}. Please verify the configuration on the host system.",
                                    "Malformed request",
                                    JOptionPane.WARNING_MESSAGE
                                )
                            }
                        }

                        reader.close()
                        client.close()
                    }
                } else {
                    JOptionPane.showMessageDialog(
                        null,
                        "Blocked connection from ${client.inetAddress.address}.",
                        "Connection warning",
                        JOptionPane.WARNING_MESSAGE
                    )
                    client.close()
                }
            }
        }
    }

    private fun isRunning(): Boolean {
        return !sock.isClosed && sock.isBound
    }
}
